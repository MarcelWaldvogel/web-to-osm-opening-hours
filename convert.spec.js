const convert = require("./convert");

describe("A suite", function() {
    it("contains spec with an expectation", function() {
        var testResult = convert("input");
        expect(testResult).toBe("output");
    });
});
